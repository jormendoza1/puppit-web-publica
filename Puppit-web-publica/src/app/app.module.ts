import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PantallaPrincipalComponent } from './components/pantalla-principal/pantalla-principal.component';
import { ListadoMascotaComponent } from './components/listado-mascota/listado-mascota.component';
import { MascotaComponent } from './components/mascota/mascota.component';
import { RequisitosComponent } from './components/requisitos/requisitos.component';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormularioComponent } from './components/formulario/formulario.component';
import { AdoptantesService } from './services/adoptantes.service';
import { DonacionComponent } from './components/donacion/donacion.component';
import { MascotasService } from './services/mascotas.service';
import { OrganizacionesService } from './services/organizaciones.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PantallaPrincipalComponent,
    ListadoMascotaComponent,
    MascotaComponent,
    RequisitosComponent,
    FooterComponent,
    FormularioComponent,
    DonacionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    AdoptantesService,
    MascotasService,
    OrganizacionesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
