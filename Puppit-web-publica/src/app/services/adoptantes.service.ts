import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class AdoptantesService {

  // URL Base
  private urlBase = environment.url_servicios_base;

  // Variables de URL Apis REST
  private apiPostAdoptantes = this.urlBase + '/api/adoptantes/';

  constructor(public http: HttpClient) { }

  postAdoptantes(adoptante: any): Observable<any> {
    const newSession = Object.assign({}, adoptante);
    return this.http.post<any[]>(this.apiPostAdoptantes, newSession, cudOptions);
  }

}
