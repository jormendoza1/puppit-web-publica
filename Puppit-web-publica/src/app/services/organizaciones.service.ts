import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class OrganizacionesService {

  // URL Base
  private urlBase = environment.url_servicios_base;

  // Variables de URL Apis REST
  private apiGetOrganizaciones = this.urlBase + '/api/ong/';
  private apiGetProvincias = this.urlBase + '/api/ong/provincia/';

  constructor(public http: HttpClient) { }

  getONGS(): Observable<any> {
    return this.http.get(this.apiGetOrganizaciones);
  }

  getONG(id: number): Observable<any> {
    return this.http.get(this.apiGetOrganizaciones + id);
  }
  
  getProvONGS(): Observable<any> {
    return this.http.get(this.apiGetProvincias);
  }
}
