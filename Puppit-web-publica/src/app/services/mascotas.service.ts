import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class MascotasService {

  // URL Base
  private urlBase = environment.url_servicios_base;

  // Variables de URL Apis REST
  private apiGetMascota = this.urlBase + '/';
  private apiGetCantMascotasXong = this.urlBase + '/ong/contar/';
  private apigetMascotaXProvTamEdad = this.urlBase + '/filtro/';

  constructor(public http: HttpClient) { }

  getMascotaXProvTamEdad(prov: string, tam: string): Observable<any>{
    return this.http.get(this.apigetMascotaXProvTamEdad + prov + '/' + tam)
  }

  getCantMascotaXong(ong: number): Observable<any> {
    return this.http.get(this.apiGetCantMascotasXong + ong);
  }

  getMascota(id: number): Observable<any> {
    return this.http.get(this.apiGetMascota + id);
  }
}