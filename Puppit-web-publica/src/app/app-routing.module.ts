import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PantallaPrincipalComponent } from './components/pantalla-principal/pantalla-principal.component';
import { ListadoMascotaComponent } from './components/listado-mascota/listado-mascota.component';
import { MascotaComponent } from './components/mascota/mascota.component';
import { RequisitosComponent } from './components/requisitos/requisitos.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { DonacionComponent } from './components/donacion/donacion.component';

const routes: Routes = [
  {
    path: '', component:  ListadoMascotaComponent
  },
  {
    path: 'p-app-components-pantalla-principal', component:  PantallaPrincipalComponent
  },
  {
    path: 'p-app-components-listado-mascota', component: ListadoMascotaComponent
  },
  {
    path: 'p-app-components-mascota/:id', component: MascotaComponent
  },
  {
    path: 'p-app-components-requisitos/:id_ong', component: RequisitosComponent
  },
  {
    path: 'p-app-components-formulario', component: FormularioComponent
  },
  {
    path: 'p-app-components-donacion', component: DonacionComponent
  },
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
