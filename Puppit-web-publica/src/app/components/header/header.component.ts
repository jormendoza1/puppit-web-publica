import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public contacto(){
    Swal.fire(
      ' Contacto ',
      'Para contactarse con nosotros enviar wsp o llamar a: +543888487898 sino escribir un mail a la siguiente direccion: proyectopuppit@gmail.com. Gracias.',
      'question'
    )
  }

}
