import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MascotasService } from 'src/app/services/mascotas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mascota',
  templateUrl: './mascota.component.html',
  styleUrls: ['./mascota.component.css']
})
export class MascotaComponent implements OnInit { 
  id: any;
  nombre!: string;
  raza!: string;
  color!: string;
  nacimiento!: string;
  tamanio!: string;
  peso!: number;
  sexo!: CharacterData;
  img!: string;
  descripcion!: string;
  id_ong: any;
  edad_actual!: number;

  constructor(private rutaActiva: ActivatedRoute, private mascotasService: MascotasService) { }

  ngOnInit(): void {
    this.id = this.rutaActiva.snapshot.params["id"];
    this.mascotasService.getMascota(this.id).subscribe( resp => {
      console.log(resp.rows);
      this.nombre = resp.rows[0].name;
      this.raza = resp.rows[0].raza;
      this.color = resp.rows[0].color;
      this.nacimiento = resp.rows[0].nacimiento;
      this.tamanio = resp.rows[0].tamanio;
      this.peso = resp.rows[0].peso;
      this.sexo = resp.rows[0].sexo;
      this.img = resp.rows[0].url_imagen;
      this.descripcion = resp.rows[0].descripcion;
      this.id_ong = resp.rows[0].id_ong;
      this.edad_actual = resp.rows[0].edad_actual_mes;
    });
  }

  public contacto(){
    Swal.fire(
      ' Contacto ',
      'Para contactarse con nosotros enviar wsp o llamar a: +543888487898 sino escribir un mail a la siguiente direccion: proyectopuppit@gmail.com. Gracias.',
      'question'
    )
  }
  
}
