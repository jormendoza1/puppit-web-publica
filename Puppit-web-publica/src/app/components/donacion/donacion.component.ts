import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Component, OnInit } from '@angular/core';
import { MascotasService } from 'src/app/services/mascotas.service';
import { OrganizacionesService } from 'src/app/services/organizaciones.service';

@Component({
  selector: 'app-donacion',
  templateUrl: './donacion.component.html',
  styleUrls: ['./donacion.component.css']
})
export class DonacionComponent implements OnInit {

  arrayOngs = <any>[];
  ong: number = 1;
  nombreONG!: string;
  cantidad!: number;
  link!: any;

  constructor(private organizacionesService: OrganizacionesService, private mascotasService: MascotasService) {}

  ngOnInit(): void {

    this.organizacionesService.getONGS().subscribe( resp => {
      console.log(resp.rows);
      this.arrayOngs = resp.rows;
    });
    this.filtrar();
  }

  public filtrar()
  {
    this.organizacionesService.getONG(this.ong).subscribe( resp => {
      console.log(resp.rows);
      this.nombreONG = resp.rows[0].nombre;
      this.link = resp.rows[0].link_donacion;
    });

    this.mascotasService.getCantMascotaXong(this.ong).subscribe(resp => {
      this.cantidad = resp.rows[0].cantidad;
    });
  }
}
