import { Component, OnInit } from '@angular/core';
import { MascotasService } from 'src/app/services/mascotas.service';
import { OrganizacionesService } from 'src/app/services/organizaciones.service';

@Component({
  selector: 'app-listado-mascota',
  templateUrl: './listado-mascota.component.html',
  styleUrls: ['./listado-mascota.component.css']
})
export class ListadoMascotaComponent implements OnInit {

  arrayMascotas = <any>[];
  arrayProv = <any>[];
  prov!: string;
  tamanio!: string;
  edad!: number;

  constructor(private mascotasService: MascotasService, private organizacionesService: OrganizacionesService) { 
  }


  ngOnInit(): void {

    this.organizacionesService.getProvONGS().subscribe( resp => {
      console.log(resp.rows);
      this.arrayProv = resp.rows;
    });

    this.tamanio = 'pequeño';
    this.prov = 'Jujuy';
    this.edad = 4;
    this.filtrar();
  }

  public filtrar()
  {

    this.mascotasService.getMascotaXProvTamEdad(this.prov, this.tamanio).subscribe(resp => {
      this.arrayMascotas = [];
      console.log(resp.rows)

      for (let index = 0; index < resp.rows.length; index++){
        if(this.edad == 5){
          if(resp.rows[index].edad_actual >= this.edad)
          {
            this.arrayMascotas.push(resp.rows[index]);
          }
        }else{
          if(resp.rows[index].edad_actual <= this.edad)
          {
            this.arrayMascotas.push(resp.rows[index]);
          }
        }
      } 
      });
  }
  
}
