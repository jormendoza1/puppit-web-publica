import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdoptantesService } from 'src/app/services/adoptantes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  acepta: boolean = false;
  edad!: any;

  apellido = '';
  nombre = '';
  dni = '';
  codigo_postal = '';
  direccion = '';
  email = '';
  celular = '';
  fechaNacimiento = '';
  sexo = '';

  adoptanteForm: FormGroup | undefined;

  constructor(private formBuilder: FormBuilder, private adoptantesService: AdoptantesService) { }

  ngOnInit(): void {
    this.adoptanteForm = this.formBuilder.group({
      apellido: ['', [Validators.required] ],
      nombre: ['', [Validators.required] ],
      dni: ['', [Validators.required] ],
      codigo_postal: ['', [Validators.required] ],
      email: ['', [Validators.required] ],
      celular: ['', [Validators.required] ],
      direccion: [ '', [Validators.required]],
      fechaNacimiento: ['', [Validators.required] ],
      sexo: [ '', [Validators.required]]
    });
  }

  enviar(){

    const nac: Date = new Date(this.fechaNacimiento);
    this.edad = ((Date.now() - <any>nac || Date.now()) / (24 * 3600 * 365.25 * 1000));
    this.edad = Math.trunc(this.edad);

    if (this.acepta == true){
      if(this.edad >= 18){
        let adoptante = {
          apellido: this.apellido,
          nombre: this.nombre,
          dni: this.dni,
          codigo_postal: this.codigo_postal,
          email: this.email,
          celular: this.celular,
          direccion: this.direccion,
          fechaNacimiento: this.fechaNacimiento,
          sexo: this.sexo
        }
    
  
  
        this.adoptantesService.postAdoptantes(adoptante).subscribe( resp => {
          console.log(resp);
        })
  
        Swal.fire({
          icon: 'success',
          title: 'Tu solicitud fue enviada.',
          html: "Hemos recibido tu solicitud de certificado de <b>adopcion</b> "
                + "Evaluaremos su solicitud en las proximas 48hs. " + 
                "<b>Nos pondremos en contacto con usted</b>",
          showConfirmButton: false,
          footer: '<a href="/">Volver a Menu Principal</a>'
        })
        
        this.apellido = '';
        this.nombre = '';
        this.dni = '';
        this.codigo_postal = '';
        this.direccion = '';
        this.email = '';
        this.celular = '';
        this.fechaNacimiento = '';
        this.sexo = '';
        this.acepta = false;
    
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Debe ser mayor de edad para poder adoptar',
        })
      }
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Debe Aceptar los terminos y condiciones',
      })
    }

  }

  
}
