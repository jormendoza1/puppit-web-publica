import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrganizacionesService } from 'src/app/services/organizaciones.service';

@Component({
  selector: 'app-requisitos',
  templateUrl: './requisitos.component.html',
  styleUrls: ['./requisitos.component.css']
})
export class RequisitosComponent implements OnInit {
  
  id_ong: any;
  terminos!: string;
  ong!: string;

  constructor(private rutaActiva: ActivatedRoute, private organizacionesService: OrganizacionesService) { }

  ngOnInit(): void {
    this.id_ong = this.rutaActiva.snapshot.params["id_ong"];
    this.organizacionesService.getONG(this.id_ong).subscribe( resp => {
      console.log(resp.rows);
      this.terminos = resp.rows[0].terminos_adopcion;
      this.ong = resp.rows[0].nombre;
    });
  }

}
